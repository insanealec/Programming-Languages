Briefly describe how to read your code.

Adding in here my thoughts on Coroutines, Actors, and Futures


	It seems as though Coroutines are good because you can tell your program to
	wait when necessary for asynchonous calls to complete
	so you can use them later. One part of the program suspends itself
	so that the scheduler can resume another part/program. This allows multiple
	things to work in parallel, and wait on another part to be done when necessary.

	Actors are cool because it allows you to launch extra threads to
	do different things at the same time
	using this in conjunction with coroutines, you could have two
	actors yield when necessary to talk to each other!

	Futures are interesting to me because of the automatic deadlock
	detection. But it's nice to write code that hasn't come back yet, but
	will be used later on and just wait until it comes back.
	sort of like AJAX in javascript and using callbacks, but much simpler!

