// your code to implement class hierarchy as given

//<Shape>
Shape := Object clone
//this line creates the setColor method
Shape color ::= "red"
Shape init := method(col,
	if(call message argCount >0,
		self setColor(col)
		//writeln("init param: color = ",self color),
		//writeln("init no param: color = ", self color)
	)
)
Shape getColor := method(
	return self color
)
Shape toString := method(
	writeln("This is a ", self type," with color ", self color,".")
)
//</Shape>

//<Circle>
Circle := Shape clone
Circle radius ::= 1.0
Circle init := method(rad, col,
	//I will get an error if they aren't all one line?
	if(call message argCount == 2) then(self setRadius(rad); super(init(col))) elseif(call message argCount == 1) then(self setRadius(rad); super(init)) else(self setRadius(1.0); super(init))
	return self
)
Circle getRadius := method(
	return self radius
)
Circle toString := method(
	super(toString)
	writeln("Radius:\t", self radius)
	writeln("Area:\t", self area)
)
Circle area := method(
	self radius squared * Number constants pi
)
//</Color>

//<Rectangle>
Rectangle := Shape clone
Rectangle width ::= 1.0
Rectangle length ::= 1.0
//Rectangle(), Rectangle(width, length) are shown in diagram, color optional
Rectangle init := method(wid, len, col,
	//I will get an error if they aren't all one line?
	if(call message argCount == 3) then(self setWidth(wid); self setLength(len); super(init(col))) elseif(call message argCount == 2) then(self setWidth(wid); self setLength(len); super(init)) else(self setWidth(1.0); self setLength(1.0); super(init))
	return self
)
Rectangle getWidth := method(
	return self width
)
Rectangle getLength := method(
	return self length
)
Rectangle toString := method(
	super(toString)
	writeln("Width:\t", self width)
	writeln("Length:\t", self length)
	writeln("Area:\t", self area)
)
Rectangle area := method(
	self width * self length
)
//</Rectangle>


function1 := method(
	"Shape Defaults:" println
	shape1 := Shape clone
	shape1 toString
	"Shape With Color Set:" println
	shape2 := Shape clone init("green")
	shape2 toString
	"" println
	
	"Circle Defaults:" println
	circle1 := Circle clone
	circle1 toString
	"Circle With Radius Set:" println
	circle2 := Circle clone init(5.0)
	circle2 toString
	"Circle With Radius And Color Set:" println
	circle3 := Circle clone init(4.0, "blue")
	circle3 toString
	"" println
	
	"Rectangle Defaults:" println
	rect1 := Rectangle clone
	rect1 toString
	"Rectangle With Length And Width Set:" println
	rect2 := Rectangle clone init(8.0, 4.7)
	rect2 toString
	"Rectangle With Length And Width And Color Set:" println
	rect3 := Rectangle clone init(9.2, 2.3, "orange")
	rect3 toString
	"" println
    
	//create the list of 10 Circle/Rectangle objects
	howMany := 9
	myList := List clone
	for(i, 0, howMany, 
		//type of object
		type := Random value(0, 1) round;
		//what the radius/length/width will be
		num := Random value(0, 10) floor;
		//set an object to circle or rectangle, then add to list
		if(type == 0,
			obj := Circle clone init(num),
			obj := Rectangle clone init(num, num)
		)
		myList append(obj);
	)
	//traverse list and compute total area
	writeln("The list of shapes:")
	total := 0
	for(i, 0, (myList size) - 1,
		total = total + myList at(i) area
		//also print out the area and type of the current object
		writeln(myList at(i) type, ", Area: ", myList at(i) area)
	)
	"Total Area of Shapes:" println
	total println
)

function2 := method(
	//Coroutine from the book
	vizzini := Object clone
 	vizzini talk := method(
		"Fezzik, are there rocks ahead?" println
		yield
		"No more rhymes now, I mean it." println
        yield
	)
 	
	fezzik := Object clone
	fezzik rhyme := method(
			yield
			"If there are, we'll all be dead." println
			yield
			"Anybody want a peanut?" println
	)

	vizzini @@talk; fezzik @@rhyme

	wait(5)
	//waiting a sufficiently large amount of time for this to complete

	//It seems as though Coroutines are good because you can tell your program to
	//wait when necessary for asynchonous calls to complete
	//so you can use them later.
)

function3 := method(
	//Actor from the book
	slower := Object clone
	faster := Object clone
	slower start := method(wait(2); writeln("slowly"))
	faster start := method(wait(1); writeln("quickly"))
	//do them in call order
	slower start; faster start
	//do them in their own threads, asynchonously
	slower @@start; faster @@start; wait(3)
	
	//actors are cool because it allows you to launch extra threads to
	//do different things at the same time
	//using this in conjunction with coroutines, you could have two
	//actors yield when necessary to talk to each other!
)

function4 := method(
	//Future from the in class
	//set up the future value
	url := Object clone
	url fetch := method(wait(3); "result from fetch")

	data := url @fetch
	writeln("Do something immediately while fetch goes on in background...")
	writeln("This will block until the result is available.")
	writeln("fetched ", data size, " bytes")
	
	//futures are interesting to me because of the automatic deadlock
	//detection. But it's nice to write code that hasn't come back yet, but
	//will be used later on and just wait until it comes back.
	//sort of like AJAX in javascript and using callbacks, but much simpler!
)

if(isLaunchScript,
    
    "Assignment #6 - Io Day 3 - S. Roy \n" println

    "Task 1" println
    "---------" println
	function1
	"" println

     // your code

    
    "Task 2" println
    "---------" println
	"Coroutine" println
	function2 
	"" println
	"Actors" println
	function3
	"" println
	"Future" println
	function4
	"" println

)
