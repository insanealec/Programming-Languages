To Run:
-------
 - scalac Scala-Assignment3.scala
 - scala ScalaAssignment3 https://www.google.com/

The URL is passed in by the user as arguments for running the program.
The comments and commits mention why things are done certain ways, in order to follow the instructions.
There are 3 output sets, first is tasks 1a-1c, second is a simplified sequential form for use in comparing to the third run, 
which is the concurrent version of task 1b in order to complete task 1d.

The times between sequential and concurrent vary wildly, 
sometimes on the google links, there is one that has different numbers of images, depending on when you visit it.

Concurrent vs Sequential times can vary from almost no difference, to the concurrent taking longer, 
to the concurrent being up to 5 times faster.

I was able to run with multiple arguments, with various sites. Some didn't work, but most did.
And the ones that didn't work, only did it once, then worked from there, or they were secure sites that didn't accept the program accessing them.
Some sites are also too large, and cause really slow execution. Google always worked well for me.

There are deprecated warnings on the compilation, but no actual compilation errors.
The deprecated warnings are all about the Actors, which is required for the assignment.
