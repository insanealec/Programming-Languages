/*
	Scala Assignment 3
	Alexander Gordon
*/
import io.Source
import scala.actors.Actor._

//PageLoader object
object PageLoader 
{
	//regexes
	val linkRegex = "<a.+?href=\"(http.+?)\".*</a>".r
	val imgRegex = "<img.+?src=\"(.+?)\".*>".r
	val scriptRegex = "<script.*</script>".r

	def load(url: String) = 
	{
		try 
		{
			Source.fromURL(url)("ISO-8859-1").mkString
		} 
		catch
		{
			case e: Exception => System.err.println(e)
			""
		}
	}

	def printPage(url: String)
	{
		val content = load(url)
		println("content = \n" + content)
	}

	def getPageSize(url: String) = load(url).length

	def getPageSizeAndLinks(url: String) = 
	{
		val content = load(url)
		//removing from here, only printing page data if printPage is called
		//couldn't see useful output when printing the page data was on
		//println("content = \n" + content)
		val links = linkRegex.findAllIn(content).matchData.toList.map(_.group(1))
		(content.length, links)
	}

	/*
		Task #1a: (8 points)Write a Scala program which takes awebpage url (say x) from the user, and then download
		webpage x. Count the number of images (i.e. "<img .../>") and scripts (i.e. "<script ...> </script>")
		present in x.
	*/
	def getPageImgs(url: String) =
	{
		val content = load(url)
		val imgs = imgRegex.findAllIn(content).matchData.toList.map(_.group(1))
		//println(imgs)
		imgs		
	}

	def getPageScripts(url: String) =
	{
		val content = load(url)
		val scripts = scriptRegex.findAllIn(content).matchData.toList.map(_.group(0))
		//println(scripts)
		scripts		
	}
}

//object wrapper so the compiler doesn't complain
object ScalaAssignment3
{
	var urls = Array[String]()

	def timeMethod(method: () => Unit)
	{
		val start = System.nanoTime
		method()
		val end = System.nanoTime
		println("Method took " + (end - start)/1000000000.0 + " seconds.")
	}

	def crawlLinks(size: Int, links: List[String]): Int =
	{
		links match 
		{
			case Nil => size
			case head :: tail => crawlLinks(size + PageLoader.getPageSize(head), tail)
		}
	}

	//Task #1b && Task #1c
	def crawlLinksForImgs(imgCount: Int, links: List[String], overFive: Int): (Int, Int) =
	{
		var isOver = 0
		links match
		{
			case Nil=> (imgCount, overFive)
			case head :: tail => {
				var curImgs = PageLoader.getPageImgs(head).size
				if (curImgs > 5)
				{
					isOver = 1
				}
				crawlLinksForImgs(imgCount + curImgs, tail, overFive + isOver)
			}
		}
	}

	def printOutput(url: String, size: Int, links: List[String], imgs: List[String], scripts: List[String], totalSize: Int, totalImgs: Int, overFive: Int)
	{
		println(url + ": size = " + size + ", links = " + links.length + 
			", images = " + imgs.length + ", scripts = " + scripts.length +", totalSize = " + totalSize +
			", totalImgs = " + totalImgs + ", links with over 5 images = " + overFive)
	}

	def conOutput(url: String, size: Int, links: List[String], imgs: List[String], scripts: List[String], totalImgs: Int)
	{
		println(url + ": size = " + size + ", links = " + links.length + 
			", images = " + imgs.length + ", scripts = " + scripts.length +
			", totalImgs = " + totalImgs)
	}

	def sequential()
	{
		for (url <- urls)
		{
			val (size, links) = PageLoader.getPageSizeAndLinks(url)
			val totalSize = crawlLinks(size, links)

			//Task #1a
			val imgs = PageLoader.getPageImgs(url)
			var originalLinkOverFive = 0
			if (imgs.size > 5)
			{
				originalLinkOverFive = 1
			}
			val (linkedImgs, overFive) = crawlLinksForImgs(0, links, originalLinkOverFive)
			val totalImgs = imgs.size + linkedImgs

			//Task #1a
			val scripts = PageLoader.getPageScripts(url)

			printOutput(url, size, links, imgs, scripts, totalSize, totalImgs, overFive)
		}
	}

	def sequential2()
	{
		for (url <- urls)
		{
			val (size, links) = PageLoader.getPageSizeAndLinks(url)
			//val totalSize = crawlLinks(size, links)

			//Task #1a
			val imgs = PageLoader.getPageImgs(url)
			val (linkedImgs, overFive) = crawlLinksForImgs(0, links, 0)
			val totalImgs = imgs.size + linkedImgs

			//Task #1a
			val scripts = PageLoader.getPageScripts(url)

			conOutput(url, size, links, imgs, scripts, totalImgs)
		}
	}

	/*
		Task #1d
		Do Task 1b again, but now you are using the Actors concept. 
		How much time does the concurrencty usage save compared to the serial run in Task 1b?
	*/
	//The task says to just do task 1b again using Actors, so I don't need to include the count > 5 or the links
	def concurrent()
	{
		val caller = self

		urls.foreach 
		{ url => 
			actor
			{
				val (size, links) = PageLoader.getPageSizeAndLinks(url)
				val imgsCollectorActor = self
				
				val imgs = PageLoader.getPageImgs(url)
				val scripts = PageLoader.getPageScripts(url)

				links.foreach(link => actor{imgsCollectorActor ! PageLoader.getPageImgs(link).size})

				var totalImgs = imgs.size 
				for (i <- 0 to imgs.length)
				{
					receive{case linkedImgs: Int => totalImgs += linkedImgs}
				}

				caller ! (url, size, links, imgs, scripts, totalImgs)
			}
		}

		for (i <- 1 to urls.length)
		{
			receive
			{
				//The @unchecked is because of type erasure on Scala, it prevents a warning about such a thing, no errors with or without it.
				case (url: String, size: Int, links: List[String @unchecked], imgs: List[String @unchecked], scripts: List[String @unchecked], totalImgs: Int) => 
					conOutput(url, size, links, imgs, scripts, totalImgs)
			}
		}
	}

	def main(args: Array[String]): Unit =
	{
		urls = urls ++ args

		println()
		println("Sequential run (including total links and number of links with images > 5):")
		println("This run shows the completed Tasks: 1a, 1b, and 1c.")
		timeMethod(sequential)
		println()
		println()

		println("Sequential run (running less code than the prior one, only to compare to the concurrent run):")
		println("This run shows the completed Tasks: 1a and 1b. So it can be compared to the Actor run for an accurate time.")
		timeMethod(sequential2)
		println()
		println()

		println("Concurrent run:")
		println("This run shows the completed Tasks: 1a, 1b, and 1d. Typically runs 1.5 to 3 times faster than the sequential run.")
		timeMethod(concurrent)
		println()
		println()
	}

}
