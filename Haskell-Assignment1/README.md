Write a brief readme file

I worked on this with Sam O'Neal

Alexander Gordon and Sam O'Neal worked together.

-----For hello.hs-----

Compile the file:

	ghc -o hello hello.hs

Then just run hello and it should print

	./hello

-----For filterThree.hs-----

Run this in the interpreter:

	ghci

	Prelude> :load filterThree.hs

Run the filterThree method with arguments to get output

	Prelude> filterThree [1, 2, 3]

-----For randReverse.hs-----

Also run this in the interpreter

	ghci

	Prelude> :load randReverse.hs

Run the main method and it will generate the required list
and then reverse it

	Prelude> main
