module Main where
	filterThree :: [Int] -> [Int]
	filterThree [] = []
	filterThree (h:t) = if (h `mod` 3 == 0) then h:filterThree t else filterThree t
