module Main where

import System.Random
import Data.List

-- The randomList Method
randomList :: Int -> StdGen -> [Int]
randomList n = take n . unfoldr (Just . random)

-- The randReverse Method
randReverse :: [Int] -> [Int]
randReverse [] = []
randReverse (h:t) = randReverse t ++ [h]

main = do
	seed <- newStdGen
	let rl = randomList 10 seed

	putStrLn "Here's the list before reversing"
	print rl

	putStrLn "And here's the list after reversing"
	return(randReverse rl)
