Sam O'Neal and Alexander Gordon are working together.

In particular, README.md includes instructions for compiling and running all the solutions to all of your exercises. It would be appreciated if this file includes the exact commands used to compile and run your code as well as examples of the output produced by your solutions.

Task 1:
All querries should be posed as:
1. eats(animal, food) for yes or no answer about what that animal eats
2. eats(animal, What) for everything eaten by that animal
3. eats(What, food) for what eats that food

Task 2:
Pose the querry like you would the question:
What is the reverse of the list [1,2,3]?
---->rev(What, [1,2,3])
        
