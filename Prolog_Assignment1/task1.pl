/* here you write your code for Task 1 */

/* animal and what kind of eater it is */
animal_type(lion, carnivore).
animal_type(giraffe, herbivore).
animal_type(chimpanzee, omnivore).
animal_type(human, omnivore).
animal_type(tiger, carnivore).
animal_type(bear, omnivore).
animal_type(dragon, carnivore).
animal_type(elephant, herbivore).
animal_type(brontosaurus, herbivore).
animal_type(swallow, herbivore).
animal_type(eagle, carnivore).

/* what a certain eater type eats */
food_type(meat, carnivore).
food_type(meat, omnivore).
food_type(plant, herbivore).
food_type(plant, omnivore).

/* food and what type of food it is */
food(ribs, meat).
food(grass, plant).
food(leaves, plant).
food(chicken, meat).
food(asparagus, plant).
food(beef, meat).
food(broccoli, plant).
food(sticks, plant).
food(pork, meat).
food(fruit, plant).

/* rule that relates an animal to the food it can eat */
eats(A, F) :- animal_type(A, T), food(F, G), food_type(G, T).
