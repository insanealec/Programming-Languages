/* here you write your code for Task 2 */
/* a list is structured like [1,2,3] */

/* count elements of a list */
count(0, []).
count(Count, [Head|Tail]) :- count(TailCount, Tail), Count is TailCount + 1.

/* sum up the elements of a list */
sum(0, []).
sum(Total, [Head|Tail]) :- sum(Sum, Tail), Total is Head + Sum.

/* average */
average(Average, List) :- sum(Sum, List), count(Count, List), Average is Sum/Count.

/* reverse a list. rev(What, [1,2,3]) to find the reverse of that list */
rev([], []).
rev(RevList, [Head|Tail]) :- rev(Left, Tail), append(Left, [Head], RevList).