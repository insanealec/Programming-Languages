/*
	Scala Assignment 2
	Alexander Gordon
*/

//Matrix class used for the task 1 of the program
class Matrix(size: Int)
{
	var mat = randomMatrix(size)
	def randomMatrix(size: Int): Array[Array[Int]] =
	{
		val r = scala.util.Random
		var myMatrix = Array.ofDim[Int](size, size)
		//loop through, adding a random value to each index between 0 and 10
		for (i <- 0 until size)
		{
			for (j <- 0 until size)
			{
				//0 through 10, includes 10
				myMatrix(i)(j) = r.nextInt(11)
			}
		}
		return myMatrix;
	}
	def printMatrix()
	{
		mat.foreach{arg =>
			arg.foreach{thing =>
				print(s"$thing	")
			}
			println()
		}
		println()
	}
}

//task 2 shapes implementation
class Shape(var color: String)
{
	//if no color is passed
	def this()
	{
		this("red")
	}

	def getColor(): String = 
	{
		return color
	}

	def setColor(col: String)
	{
		color = col
	}

	override def toString(): String = 
	{
		return "Shape: Color = "+color
	}

	def area(): Double = 
	{
		return 0
	}
}
class Circle(color: String, var radius: Double) extends Shape(color)
{
	val pi = 3.14159
	def this(radius: Double)
	{
		this("red", radius)
	}

	def this()
	{
		this(1.0)
	}

	def getRadius(): Double = 
	{
		return radius
	}

	def setRadius(rad: Double)
	{
		radius = rad
	}

	override def toString(): String =
	{
		return "Circle: Color = "+color+", Radius = "+ radius+", Area = "+this.area
	}

	override def area(): Double =
	{
		return radius * pi * pi
	}
}
class Rectangle(color: String, var width: Double, var length: Double) extends Shape(color)
{
	def this(width: Double, length: Double)
	{
		this("red", width, length)
	}

	def this()
	{
		this(1.0, 1.0)
	}

	def getWidth(): Double = 
	{
		return width
	}

	def setWidth(wid: Double)
	{
		width = wid
	}

	def getLength(): Double = 
	{
		return length
	}

	def setLength(len: Double)
	{
		length = len
	}

	override def toString(): String =
	{
		return "Rectangle: Color = "+color+", Width = "+ width+", Length = "+ length+", Area = "+this.area
	}

	override def area(): Double =
	{
		return width * length
	}
}

object ScalaAssignment2
{
	/*
	Task #1: (25 points)Write a Scala program which can performmatrix addition. 
	You need to write a matAdd function that should take two parameters –Matrix P 
	and Matrix Q – and return the result matrix –Matrix R.
	Test your program by generating a variety of randommatrices that are 
	filled with random integers between
	0 and 10. Testwith matrix sizes of 5x5, and 10x10, and showthe results.
	You areNOT allowed to use/include
	any library in your implementation to generate or add matrices.
	For this program, you must also create at least 2 helper functions
	 – randomMatrix and printMatrix.
	These filesmay be in separate files, classes, etc. and should followthe 
	paradigms of Scala. The randomMatrix
	will generate and return a matrix filled with random values and 
	printMatrix will print a well-formatted	matrix.
	*/
	def matAdd(matP: Matrix, matQ: Matrix): Matrix =
	{
		//matAdd is separate so that there's no confusion about whos matrix is whos
		var p1 = matP.mat.size
		var q1 = matP.mat(0).size
		var p2 = matQ.mat.size
		var q2 = matQ.mat(0).size
		//create the new matrix (random values will be overriden)
		var matR = new Matrix(p1)
		if (p1 != p2 && q1!= q2)
		{
			println("Matrix sizes do not match! They cannot be added!")
			for (i <- 0 until p1)
			{
				for (j <- 0 until p1)
				{
					matR.mat(i)(j) = 0
				}
			}
		}
		else
		{
			for (i <- 0 until p1)
			{
				for (j <- 0 until p1)
				{
					matR.mat(i)(j) = matP.mat(i)(j) + matQ.mat(i)(j)
				}
			}
		}
		return matR;
	}

	/*
	Task #2: (25 points) Implement the classes as shown in the class hierarchy 
	diagram below. Now write a test function (function1) that demonstates 
	that you have implemented the classes and the inheritance
	properly. In addition, in function1, create a list l, 
	and randomly populate the list with 10 objects among
	which some are circles, and others are rectangles. 
	Then, traverse the list l, and compute the total "area" of
	these shapes.
	*/
	def function1()
	{
		var size = 10
		var r = scala.util.Random
		//var objects = new Array[Shape](size)
		var l = List[Shape]()
		var total = 0.0
		var area = 0.0
		
		//set the objects
		for (i <- 0 until size)
		{
			var choice = r.nextInt(2)
			if (choice == 0)
			{
				var ob = new Circle(r.nextInt(size))
				//objects(i) = ob
				l ::= ob
			}
			else
			{
				var ob = new Rectangle(r.nextInt(size), r.nextInt(size))
				//objects(i) = ob
				l ::= ob
			}
		}

		println(s"Traverse a list of $size random objects:")
		l.foreach{arg =>
			area = arg.area
			total = total + area
			println(arg.toString)
		}

		println("Total Area of Objects: "+total)
	}

	def main(args: Array[String]): Unit =
	{
		//Task 1
		println("Matrix P of size 5:")
		var P = new Matrix(5)
		P.printMatrix
		println("Matrix Q of size 5:")
		var Q = new Matrix(5)
		Q.printMatrix
		println("Matrices added (5x5):")
		var R = matAdd(P, Q)
		R.printMatrix
		println()
		println()
		println("Matrix matP of size 10:")
		var matP = new Matrix(10)
		matP.printMatrix
		println("Matrix matQ of size 10:")
		var matQ = new Matrix(10)
		matQ.printMatrix
		println("Matrices added( 10x10):")
		var matR = matAdd(matP, matQ)
		matR.printMatrix
		println()
		println()
		println("Mis-matched matrix sizes (2x2)+(3x3):")
		var mP = new Matrix(2)
		println("2x2")
		mP.printMatrix
		var mQ = new Matrix(3)
		println("3x3")
		mQ.printMatrix
		var mR = matAdd(mP, mQ)
		mR.printMatrix
		println()
		println()

		//Task 2
		println("Shape with no params")
		var s1 = new Shape
		println(s1.toString)
		println("Color changed")
		s1.setColor("mauve")
		println(s1.toString)
		println("Shape with color set")
		var s2 = new Shape("orange")
		println(s2.toString)
		println()

		println("Circle with no params")
		var c1 = new Circle
		println(c1.toString)
		println("Radius changed")
		c1.setRadius(3.3)
		println(c1.toString)
		println("Circle with radius set")
		var c2 = new Circle(4.5)
		println(c2.toString)
		println("Circle with all set")
		var c3 = new Circle("blue", 4.5)
		println(c3.toString)
		println()

		println("Rectangle with no params")
		var r1 = new Rectangle
		println(r1.toString)
		println("Length changed")
		r1.setLength(9.9)
		println(r1.toString)
		println("Rectangle with Length and Width set")
		var r2 = new Rectangle(2.9, 8.3)
		println(r2.toString)
		println("Rectangle with all set")
		var r3 = new Rectangle("green", 9.1, 7)
		println(r3.toString)
		println()
		function1
	}
}
