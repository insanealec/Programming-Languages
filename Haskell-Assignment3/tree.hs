--Task #2: (10 points) Study the recursive type Tree discussed in the textbook. Also, study the Maybe monad
--given in the textbook. Furthermore, refer to the tutorial at http://www.seas.upenn.edu/~cis194/lectures/
--07-monads.html and study the zipTree function. Note that zipTree function zips two binary trees together
--by applying a function f to the values at each node. However, the function should fail if the structure
--of the two trees are different. Now you need to create example trees with integer node values. Considering
--the operation f is usual integer addition, apply zipTree on your two trees. In one of your example, the
--operation zipTree may succeed while in another it should return Nothing.


module Main where
  --tree type and zipTree taken from link above
  data Tree a = Node (Tree a) a (Tree a)
            | Empty
              deriving (Show)

  zipTree :: (a -> b -> c) -> Tree a -> Tree b -> Maybe (Tree c)
  zipTree _ (Node _ _ _) Empty = Nothing
  zipTree _ Empty (Node _ _ _) = Nothing
  zipTree _ Empty Empty        = Just Empty
  zipTree f (Node l1 x r1) (Node l2 y r2) = do
      l <- zipTree f l1 l2
      r <- zipTree f r1 r2
      return $ Node l (f x y) r