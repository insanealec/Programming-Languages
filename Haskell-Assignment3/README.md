Alexander Gordon

card.hs (task 1)
  part a:
    compareCards takes 2 cards and returns the card of a higher value
      *Main> compareCards Jack Queen
      Queen
      *Main> compareCards King Ten
      King

  part b:
    totalValue takes a hand, then returns the total value of all cards in the hand.
      *Main> totalValue [(King, Spades), (Queen, Hearts), (Jack, Spades)]
      9
      *Main> totalValue [(Ten, Hearts), (Ace, Spades)]
      6

  part c:
    compareHands which takes two Hands and tells which Hand has higher value.
      *Main> compareHands [(Ten, Spades), (Ace, Spades), (Queen, Hearts)] [(King, Spades), (King, Hearts)]
      [(Ten,Spades),(Ace,Spades),(Queen,Hearts)]
      *Main> compareHands [(Ten, Spades), (Ace, Spades), (Queen, Hearts)] [(King, Spades), (King, Hearts), (Jack, Spades)]
      [(King,Spades),(King,Hearts),(Jack,Spades)]

tree.hs
  Took code from link as told to in class. Input/Output below. Along with analysis.
  It seems as though the code from that website didn't work:
    *Main> let tree1 = Node (Node (Empty) 5 (Empty)) 20 (Node (Empty) 6 (Empty))
    *Main> tree1
    Node (Node Empty 5 Empty) 20 (Node Empty 6 Empty)
    *Main> let tree2 = Node (Node (Empty) 7 (Empty)) 30 (Empty)
    *Main> zipTree tree1 tree2
    
    <interactive>:9:9:
        Couldn't match expected type `Integer -> b -> c'
                    with actual type `Tree Integer'
        Relevant bindings include
          it :: Tree b -> Maybe (Tree c) (bound at <interactive>:9:1)
        In the first argument of `zipTree', namely `tree1'
        In the expression: zipTree tree1 tree2
    *Main> let tree3 = Node (Node (Empty) 8 (Empty)) 4 (Empty)
    *Main> zipTree tree1 tree3
    
    <interactive>:11:9:
        Couldn't match expected type `Integer -> b -> c'
                    with actual type `Tree Integer'
        Relevant bindings include
          it :: Tree b -> Maybe (Tree c) (bound at <interactive>:11:1)
        In the first argument of `zipTree', namely `tree1'
        In the expression: zipTree tree1 tree3

  In practice monads, seem okay, but it's really not that hard to read something like crawl( stagger (stagger x)).
  And Monads aren't really that much more understandable than just doing the above, you have to know a lot about them to get them to work.
  I also haven't really seen any examples in Haskell where we really need immutable data types that badly.