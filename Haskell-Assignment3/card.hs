--Task #1: (20 points) Refer to the user-defined types Card and Hand in the textbook (cards-with-show.hs).
--Also, see the value and cardvalue function therein. Now you need to do the following: (a) write a function
--named compareCards which takes two Cards and returns the higher value Card. (b) write a function
--named totalValue which takes a Hand and returns the sumof all values of cards in that hand. (c) write a
--function named compareHands which takes two Hands and tells which Hand has higher value.


module Main where 
  data Suit = Spades | Hearts deriving (Show) 
  data Rank = Ten | Jack | Queen | King | Ace deriving (Show)
  type Card = (Rank, Suit) 
  type Hand = [Card]

  value :: Rank -> Integer
  value Ten = 1 
  value Jack = 2 
  value Queen = 3
  value King = 4 
  value Ace = 5

  cardValue :: Card -> Integer
  cardValue (rank, suit) = value rank

  numCards :: Hand -> Int
  numCards h = length h

  --part a
  compareCards :: Monad m => Rank -> Rank -> m Rank
  compareCards a b = if (value a > value b) then return a else return b

  --part b
  totalValue :: Hand -> Integer
  totalValue [] = 0
  totalValue ((rank, suit):t) = (value rank) + (totalValue t)

  --part c
  compareHands :: Monad m => Hand -> Hand -> m Hand
  compareHands h1 h2 = if (totalValue h1 > totalValue h2) then return h1 else return h2