player(a).
player(b).
player(c).
player(d).
player(e).
player(f).

match(X, Y) :-
	player(X),
	player(Y),
	X @< Y.

tennis :-
	forall(match(X, Y), write((X, Y, '\n'))),
	open('output.txt', write, Stream),
	forall(match(X, Y), write(Stream, (X, Y, '\n'))),
	close(Stream).