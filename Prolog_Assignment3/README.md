Task 1:
| ?- pathFromRoot(dave).
zeb -> john_boy_sr -> john_boy_jr -> bill -> john -> joey -> george -> mike -> matt -> jared -> adam -> mark -> andy -> dave

Task 2:
| ?- myDelete(a, [b,a,d,a,a,b], R).

R = [b,d,b] ? 

Task 3:
| ?- prune([c,1,b,2,c,3,c,4,b], Y).

Y = [c,1,b,2,3,4] ? 

README.md includes instructions for compiling and running your solutions. It would be appreciated if this file includes the exact commands used to compile and run your code as well as examples of the output produced by your solutions.