/*
Write a Prolog rule myDelete(A, List, Res) which deletes all occurrences of A from
the List to produce result Res. As an example, running query myDelete(a, [b,a,d,a,a,b], R). should
give us R � [b,d,b]. Test your implementation of the rule with different querries.
*/

myDelete(A, [], []).
myDelete(A, [Head|Tail], Res) :- Head = A, myDelete(A, Tail, Res).
myDelete(A, [Head|Tail], [Head|Res]) :- myDelete(A, Tail, Res).