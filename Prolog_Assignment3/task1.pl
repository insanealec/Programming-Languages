/*
Given a family�s ancestry facts (e.g. facts about zeb, john_boy_sr, john_boy_jr as in
the textbook), write a Prolog program to print the ancestry path from the root ancestor to a person P. In
particular, you need to design a rule pathFromRoot(P) which will print �root �> level1Node �> level2Node
... �> P� (e.g. zeb �> john_boy_sr �> john_boy_jr). Test your program on a family of (at least) 10 members,
which has a great grandfather and his descendants. For the sake of simplicity, we are only considering male
members in the ancestry tree (like the �ancestor� and �father� predicate in the textbook). Hint: you can use
the �write� feature of Prolog to print the path.
*/

father(zeb, john_boy_sr).
father(john_boy_sr, john_boy_jr).
father(john_boy_jr, bill).
father(bill, john).
father(john, joey).
father(joey, george).
father(george, mike).
father(mike, matt).
father(matt, jared).
father(jared, adam).
father(adam, mark).
father(mark, andy).
father(andy, dave).

ancestor(X, Y) :- father(X, Y).
ancestor(X, Y) :- father(X, Z), ancestor(Z, Y).

/* keep writing the previous and the line until zeb, then just write zeb */
pathFromRoot(P) :- father(F, P),
	pathFromRoot(F),
	write(' -> '),
	write(P).
pathFromRoot('zeb') :- write('zeb').