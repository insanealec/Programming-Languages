/*
Write a Prolog rule prune(X,Y) which removes multiple occurrences (if any) of elements
from List X to produce result List Y. For example,
?- prune([c,1,b,2,c,3,c,4,b], Y).
Y = [c,1,b,2,3,4]
You should design the rule in such a way that Y will have remaining elements in the order as in X. Test your
implementation of the rule with different querries.
*/

prune([], []).
prune([Head|Tail], [Head|Y]) :- prune(Head, Tail, NewTail),
	prune(NewTail, Y).
	
prune(X, [], []).
prune(X, [X|Tail], Y) :- prune(X, Tail, Y).
prune(X, [Head|Tail], [Head|Y]) :- X \= Head,
	prune(X, Tail, Y).