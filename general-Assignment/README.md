Alexander Gordon

Note: had to change the first implementation to have "fib 2 = 1" so that it would be equal to the other implementations.
The answers below are aware of this.

(a) 5 points. Copy all four implementations to your computer and run and show
sample results.

First function:
	*Main> fib 30
	832040
	(3.48 secs, 382,601,544 bytes)
fibResult/fibTouple implementation:
	*Main> fib2 30
	832040
	(0.00 secs, 1,810,600 bytes)
fibNthPair implementation:
	*Main> fib3 30
	832040
	(0.00 secs, 1,680,072 bytes)
fibNth/lazyFib implementation:
	*Main> fibNth 30
	832040
	(0.00 secs, 0 bytes)


(b) 5 points. For each implementation, report the highest n for which your computer was able
to compute fib n within reasonable amount of time.

I have about 6.8 useable GB of RAM in my laptop.
First (simple):
	*Main> fib 36
	14930352
	(58.97 secs, 6,715,694,072 bytes)

Cannot display result for next functions, it's too large.
Second (touple):
	*Main> fib2 1000000
	(160.35 secs, 44,424,180,168 bytes)

Third (pair):
	*Main> fib3 1000000
	(158.73 secs, 44,564,273,632 bytes)

This one crashes GHCi at 1000000. And holds up past 500000.
Fourth (lazy):
	*Main> fibNth 500000
	(110.31 secs, 11,172,469,032 bytes)


(c) 7 points. Which implementation performed worst?
Explain why, i.e., what in its code design made it the worst. Detailed explanation is needed.
	
The worst implementation was the simplest. This is because for each number it goes up by about power of 2 each time, I think.
As shown below it's very ineffecient because it finds the same number many times. Just for fib 5 it has to
go all the way down to a base case about 5 times, then work it's way back up. It finds the same number very many times, 
unnecessarily adding to the stack.
	EG:
	fib 5
	(fib 5-1 + fib 5-2)
	(fib 4 + fib 5-2)
	((fib 4-1 + fib 4-2) + fib 5-2)
	((fib 3 + fib 4-2) + fib 5-2)
	(((fib 3-1 + fib 3-2) + fib 4-2) + fib 5-2)
	(((fib 2 + fib 3-2) + fib 4-2) + fib 5-2)
	(((1 + fib 3-2) + fib 4-2) + fib 5-2)
	(((1 + fib 2) + fib 4-2) + fib 5-2)
	(((1 + 1) + fib 4-2) + fib 5-2)
	((2 + fib 4-2) + fib 5-2)
	((2 + (fib 2)) + fib 5-2)
	((2 + 1) + fib 5-2)
	(3 + fib 5-2)
	(3 + fib 3)
	(3 + (fib 3-1 + fib 3-2))
	(3 + (fib 2 + fib 3-2))
	(3 + (1 + fib 3-2))
	(3 + (1 + fib 1))
	(3 + (1 + 1))
	(3 + 2)
	(5)


(d) 8 points. For
each implementation, explain the steps of computing fib 5. You need to go over the code and explain. Wherever
possible, draw a diagram showing the items in the execution stack while computing fib 5.

First:
For each number n, it has to reach a base case at least n times to come back up and add everything together.
This is very inefficient because it reaches the base many times, and repeats the same adition/finding the same fib many times.
	fib 5
	(fib 5-1 + fib 5-2)
	(fib 4 + fib 5-2)
	((fib 4-1 + fib 4-2) + fib 5-2)
	((fib 3 + fib 4-2) + fib 5-2)
	(((fib 3-1 + fib 3-2) + fib 4-2) + fib 5-2)
	(((fib 2 + fib 3-2) + fib 4-2) + fib 5-2)
	(((1 + fib 3-2) + fib 4-2) + fib 5-2)
	(((1 + fib 2) + fib 4-2) + fib 5-2)
	(((1 + 1) + fib 4-2) + fib 5-2)
	((2 + fib 4-2) + fib 5-2)
	((2 + (fib 2)) + fib 5-2)
	((2 + 1) + fib 5-2)
	(3 + fib 5-2)
	(3 + fib 3)
	(3 + (fib 3-1 + fib 3-2))
	(3 + (fib 2 + fib 3-2))
	(3 + (1 + fib 3-2))
	(3 + (1 + fib 1))
	(3 + (1 + 1))
	(3 + 2)
	5

Second:
At any point there's not really any stack to return to, other than knowing that the final result of fibTuple is pulled out by fibResult.
Only inefficient because it goes one step further and has one extra addition.
Only has to do math n times.
Keeps adding x and y together and subtracting from n until n = 0, at which point the touple is returned.
Y keeps replacing x on each recursive call, so that finally the result is in the first position.
	fib2 5
	fibResult(fibTuple (0, 1, 5))
	fibResult(fibTuple (1, 0+1, 5-1)) //since it doesn't have to return the stack, I'll shorthand the math from now on
	fibResult(fibTuple (1, 1, 4))
	fibResult(fibTuple (1, 2, 3))
	fibResult(fibTuple (2, 3, 2))
	fibResult(fibTuple (3, 5, 1))
	fibResult(fibTuple (5, 8, 0))
	fibResult((5,8,0))
	5


Third:
Very similar to the second implementation, but strangely enough it seems to build a larger stack.
It keeps expanding fibNthPair until it returns the base case, then it keeps calling fibNextPair until that returns the final time,
where fib3 uses fst to grab the first index of the returned touple.
	fib3 5
	fst . fibNthPair 5
	fst . (fibNextPair (fibNthPair 4))
	fst . (fibNextPair (fibNextPair (fibNthPair 3)))
	fst . (fibNextPair (fibNextPair (fibNextPair (fibNthPair 2))))
	fst . (fibNextPair (fibNextPair (fibNextPair (fibNextPair (fibNthPair 1)))))
	fst . (fibNextPair (fibNextPair (fibNextPair (fibNextPair (1, 1)))))
	fst . (fibNextPair (fibNextPair (fibNextPair (1, 2))))
	fst . (fibNextPair (fibNextPair (2, 3)))
	fst . (fibNextPair (3, 5))
	fst . (5, 8)
	5


Fourth:
lazyFib keeps going, it's lazy, so you have to tell how many values you want for it.
Take (n) does this, so it keeps going until it gets the nth value, which is the result we want.
lazyFib functions similarly to the last two, but instead it appends the x to a list, then puts y in the x spot adds x and y, then calls itself again.
Once it returns the list, it then drops the first n-1, in this case the first 4.
Then it gets the head of a single-value list, which is our result, 5.
	fibNth 5
	head (drop (4) (take (5) fib4))
	head (drop (4) (take (5) (lazyFib 1 1)))
	head (drop (4) (take (5) [1]:(lazyFib 1 2)))
	head (drop (4) (take (5) [1, 1]:(lazyFib 2 3)))
	head (drop (4) (take (5) [1, 1, 2]:(lazyFib 3 5)))
	head (drop (4) (take (5) [1, 1, 2, 3]:(lazyFib 5 8)))
	head (drop (4) (take (5) [1, 1, 2, 3, 5]))
	head (drop (4) [1, 1, 2, 3, 5])
	head [5]
	5

