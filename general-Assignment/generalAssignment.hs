module Main where
    --from part 1 of Haskell
    --simple and inefficient
    fib :: Integer -> Integer
    fib 0 = 0
    fib 1 = 1
    fib 2 = 1
    fib x = fib (x - 1) + fib (x - 2)

    --second one that uses touples
    --fibResult grabs the right result from the fibTuple
    --fib2 makes it easier to grab the result
    fibTuple :: (Integer, Integer, Integer) -> (Integer, Integer, Integer)
    fibTuple (x, y, 0) = (x, y, 0)
    fibTuple (x, y, index) = fibTuple (y, x + y, index - 1)
    
    fibResult :: (Integer, Integer, Integer) -> Integer
    fibResult (x, y, z) = x
    
    fib2 :: Integer -> Integer
    fib2 x = fibResult (fibTuple (0, 1, x))

    --third implementation using pairs
    --uses tail recursion and fst to grab the first of the touple
    fibNextPair :: (Integer, Integer) -> (Integer, Integer)
    fibNextPair (x, y) = (y, x + y)
    

    fibNthPair :: Integer -> (Integer, Integer)
    fibNthPair 1 = (1, 1)
    fibNthPair n = fibNextPair (fibNthPair (n - 1))
    
    fib3 :: Integer -> Integer
    fib3 = fst . fibNthPair

    --fourth implementation lazy fib
    --Uses lazy evaluation, you can take 5(fib) and it gives a list of the first 5 fib numbers
    --fibNth gives the x'th fib number
    lazyFib x y = x:(lazyFib y (x + y))
    
    fib4 = lazyFib 1 1
    
    fibNth x = head (drop (x - 1) (take (x) fib4))
