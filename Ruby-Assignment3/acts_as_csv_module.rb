#acts_as_csv_module.rb

module ActsAsCsv
  class CsvRow
    def initialize(headers, columns)
      @headers = headers
      @columns = columns
    end
	
	#add a method_missing function
    def method_missing(sym, *args, &block) 
      col_index = @headers.index(sym.to_s)
	  unless col_index.nil?
		@columns[col_index] 
	  end
    end
  end
  
  def self.included(base)
    base.extend ClassMethods
  end
  
  module ClassMethods
    def acts_as_csv
      include InstanceMethods
    end
  end
  
  module InstanceMethods
    def read
      @csv_contents = []
      filename = File.dirname(__FILE__) + '/mycsv.csv'
      file = File.new(filename)
      @headers = file.gets.chomp.split(', ')
      file.each do |row|
        @csv_contents << row.chomp.split(', ')
      end
    end
	
	#add an each function
    def each(&block)
      @csv_contents.each do |row|
        yield CsvRow.new(@headers, row)
      end
    end
	
    attr_accessor :headers, :csv_contents
    def initialize
      read
    end
  end
end

class MyCsv # no inheritance! You can mix it in
  include ActsAsCsv
  acts_as_csv
end