#require './MyCsv'
require_relative 'acts_as_csv_module.rb'

def function1()  
    m = MyCsv.new

    m.each do |row| 
        puts "First Item: #{row.songTitle}"
        puts "Second Item: #{row.artistName}"
        puts "Third Item: #{row.duration}"
        puts ""
    end
end


if __FILE__ == $0

   function1

end
