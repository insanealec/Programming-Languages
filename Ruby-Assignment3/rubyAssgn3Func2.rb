#Alexander Gordon
class Shape
	attr_accessor :color, :filled
	def initialize(args)
		#defaults specified in diagram
		@color = "red"
		@filled = true
		#if the parameter exists, then set it
		@color = args["color"] unless args["color"].nil?
		@filled = args["filled"] unless args["filled"].nil?
	end

	def toString
		return "Color: #{@color}, Filled: #{@filled}"
	end
end

class Circle < Shape	
	attr_accessor :radius
	def initialize(args)
		#initialize the elements in the parent
		super
		#default
		@radius = 1.0
		#set
		@radius = args["radius"].to_f unless args["radius"].nil?
	end

	def toString
		#use the toString og the parent, plus more information
		return super + ", Radius: #{@radius}"
	end
	
	def getArea
		#pi * r^squared
		return 3.14159 * @radius * @radius
	end

	def getPerimeter
		#A.K.A.: Circumference
		#2 * pi * r
		return 2 * 3.14159 * @radius
	end


end
	
class Rectangle < Shape
	attr_accessor :length, :width
	def initialize(args)
		#initialize parent elements
		super
		#defaults
		@length = 1.0
		@width = 1.0
		#set if they're passed
		@length = args["length"].to_f unless args["length"].nil?
		@width = args["width"].to_f unless args["width"].nil?	
	end
	
	def toString
		#use the parent function plus more info
		return super + ", Length: #{@length}, Width: #{@width}"
	end

	def getArea
		#width * length
		return @length * @width
	end

	def getPerimeter
		#(2*length)+(2*width)
		return (2 * @length) + (2 * @width)
	end
end

class Square < Rectangle
	attr_accessor :side
	def initialize(args)
		#parent
		super
		#if one of these isn't the default '1.0' value, then use that as the side
		if @length == 1.0
			@length = @width
		else
			@width = @length
		end
		#after done 'squaring' the rectangle, so set the side to one of them for default
		@side = @length
		#if side is passed in, overwrite everything
		unless args["side"].nil?
			@side = args["side"].to_f
			@length = @side
			@width = @side
		end
	end
	
	def toString
		#the side is the same as length and width now, so just show parent info
		#also show the side info anyway
		return super + ", Side: #{@side}"
	end

	def setLength(side)
		#use this to manually set the length, changes every other side as well
		@side = side.to_f
		@length = @side
		@width = @side
	end
	
	def setWidth(side)
		#use this to manually set the width, changes every other side as well
		setLength(side)
	end
	
	#other functions in rectangle are now the same, since everything is set to be same side
end

def function2()
# test your implementation

	shape1 = Shape.new({})
	puts "Display default Shape"
	puts shape1.toString 
	
	shape2 = Shape.new({"color" => "blue", "filled" => "false"})
	puts "Display Shape with both values set"
	puts shape2.toString 
	
	shape3 = Shape.new({"color" => "yellow"})
	puts "Display Shape with color set"
	puts shape3.toString 
	
	shape3.filled = "false"
	puts "Display altered filled value of Shape"
	puts shape3.filled
	puts ""	

	circle1 = Circle.new({})
	puts "Display default Circle"
	puts circle1.toString 
	
	circle2 = Circle.new({"color" => "orange", "radius" => "7.5", "filled" => "false"})
	puts "Display values passed to Circle"
	puts circle2.toString 

	circle2.radius = 100
	puts "Display altered radius of Circle"
	puts circle2.radius 
	
	puts "Area of Circle"
	puts circle2.getArea

	puts "Perimeter of Circle (circumference)"
	puts circle2.getPerimeter
	puts ""

	rectangle1 = Rectangle.new({})
	puts "Default Rectangle"
	puts rectangle1.toString

	rectangle2 = Rectangle.new({"color" => "green", "filled" => "false", "width" => "9", "length" => "7"})
	puts "New Rectangle passed values"
	puts rectangle2.toString

	rectangle2.width = 34
	rectangle2.length = 2
	puts "Changed Length and Width"
	puts rectangle2.width
	puts rectangle2.length

	puts "Rectangle Area"
	puts rectangle2.getArea
	
	puts "Rectangle Perimeter"
	puts rectangle2.getPerimeter
	puts ""
	
	square1 = Square.new({})
	puts "Default Square"
	puts square1.toString
	
	square2 = Square.new({"color" => "cyan", "filled" => "false", "side" => "6"})
	puts "Square with side set"
	puts square2.toString
	
	puts "Square Area"
	puts square2.getArea
	
	puts "Square Perimeter"
	puts square2.getPerimeter
	
	square3 = Square.new({"length" => "1.0", "width" => "10"})
	puts "Square with length and width set"
	puts square3.toString
	
	square3.setWidth(8.1)
	puts "Change the Width"
	puts square3.toString
	
	square3.setLength(17.5)
	puts "Change the Length"
	puts square3.toString
		 
end


if __FILE__ == $0

   function2

end
