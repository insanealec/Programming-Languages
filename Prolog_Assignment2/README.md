Task 1:
Input: largest(What, [1,2,3]).
to get back the largest element of the list
Output: What = 3
Input: largest(10, What).
Output: What = [10]

Task 2:
Input: isort([1,3,2], What).
Output: What = [1,2,3]

Task 3:
Write a recursive predicate findFlight/2 that tells us when we an travel by plane between two towns.
Input: findFlight(houston, detroit).
Output: true


README.md includes instructions for compiling and running your solutions. It would be appreciated if this file includes the exact commands used to compile and run your code as well as examples of the output produced by your solutions.