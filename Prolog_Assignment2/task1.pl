/* here you write your code for Task 1 */
/* write a rule that will find the largest element of a list */
/* do like: largest(What, [1,2,3])  to find the largest value in the supplied list */
largest(Head, [Head|[]]).
largest(Max, [Head|Tail]) :-largest(Max, Tail), Max >= Head.
largest(Head, [Head|Tail]) :- largest(Max, Tail), Max < Head.