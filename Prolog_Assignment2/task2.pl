/* here you write your code for Task 2 */
/* insertion sort, based on Accumulator */
isort(List, S) :- insert(List, [], S).
insert([], A, A).
insert([Head|Tail], A, S) :- insertion(Head, A, NewA), insert(Tail, NewA, S).

insertion(X, [], [X]).
insertion(X, [Head|Tail], [X,Head|Tail]) :- X=<Head.
insertion(X, [Head|Tail], [Head|NewTail]) :- X>Head, insertion(X, Tail, NewTail).
