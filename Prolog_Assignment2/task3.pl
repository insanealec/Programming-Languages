/* here you write your code for Task 3 */
/* consider the following knowledge base */
nonStopFlight(pittsburgh, cleveland).
nonStopFlight(philadelphia, pittsburgh).
nonStopFlight(columbus, philadelphia).
nonStopFlight(sanFrancisco, columbus).
nonStopFlight(detroit, sanFrancisco).
nonStopFlight(toledo, detroit).
nonStopFlight(houston, toledo).

/* write a recursive predicate findFlight/2 that tells us when we can travel by plane between two towns */
/* thank you for answering my question */
/* findFlight(toledo, sanFrancisco) will evaluate to "true". 
See that nonStopFlight does not give this this result. */
findFlight(X, Y) :- nonStopFlight(X, Y).
findFlight(X, Y) :- nonStopFlight(X, Z), findFlight(Z, Y).