def function1
    puts "This is my first Ruby function"
end

def function2
	puts "Enter a line to be saved to file."
	line1 = gets
	aFile = File.new("filename", "w") 
	aFile.write(line1) 
	aFile.close
end

def function3
	puts "What is your favorite language?"
	line2 = gets
	x = /Perl|Python/
	y = /Ruby|ruby/
	if line2 =~ x
		puts "you are interesting"
	elsif line2 =~ y
		puts "you are awesome"
	end
end
def function4
    z = 10
	while z < 100
		puts z
		z = z + 1
	end
end
def function5
    # Randomly pick a number between 1 and 10. After selecting this value, have the
    # computer randomly guess what value you chose by randomly generating guesses between 1 and 10 until
    # the correct number is guessed. After each guess, display wether the guess was “Too High” or “Too Low”.
	puts "Pick a number between 1 and 10."
	line3 = gets
	same = false
	numTries = 1
	while !same
		r = rand(1..10)
		if line3.to_i == r
			puts "It took the computer #{numTries} tries!"
			same = true
		elsif line3.to_i > r
			puts "Too Low"
		elsif line3.to_i < r
			puts "Too High"
		end
		numTries = numTries + 1
	end
end
if __FILE__ == $0
   
    function1
    puts ""
    function2
    puts ""
    function3
    puts ""
    function4
    puts ""
    function5
    puts ""

end


