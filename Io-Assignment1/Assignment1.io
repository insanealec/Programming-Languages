function1 := method (
	"Hello, world." println
)

function2 := method(haystack, needle,
	"Finding \"#{needle}\" in \"#{haystack}\"" interpolate println
	index := haystack findSeq(needle)
	if(index isNil, 
		"#{haystack} does not contain #{needle}" interpolate println, 
		"#{haystack} contains #{needle}" interpolate println
		"#{needle} is found at index #{index}" interpolate println
	)
)

randArray := method(howMany,
	retArr := List clone
	for(i, 0, howMany, 
		num := Random value(300) floor;
		retArr append(num);
	)
	retArr
}

function3 := method(myList,
	isFirst := true
	forward := myList sort
	"Sorted: " print
	for(i, 0, (myList size) - 1, 
		if(isFirst,
			"" println
			isFirst = false,
			", " print
		)
		"#{forward at(i)}" interpolate print
	)
	"\n" println
	
	isFirst = true
	rev := myList sort reverse
	"Reverse Sorted: " print
	for(i, 0, (myList size) - 1, 
		if(isFirst,
			"" println
			isFirst = false,
			", " print
		)
		"#{rev at(i)}" interpolate print
	)
	"\n" println
	
	isFirst = true
	//this refers to index, start at 1, and print every 2nd
	"Odd only: " print
	for(i, 1, (myList size) - 1, 2, 
		if(isFirst,
			"" println
			isFirst = false,
			", " print
		)
		"#{forward at(i)}" interpolate print
	)
	
	"\n" println
)

function4 := method(myList,
	isFirst := true
	"Unsorted: " print
	for(i, 0, (myList size) - 1, 
		if(isFirst,
			"" println
			isFirst = false,
			", " print
		)
		"#{myList at(i)}" interpolate print
	)
	"\n" println

	for(i, 0, (myList size) - 1,
		min := i;
		for(j, i + 1, (myList size) - 1, 
			if((myList at(j) < myList at(min)), 
				min = j,
				"false"
			)
		)
		if(min != i,
			temp := myList at(i)
			myList atPut(i, myList at(min))
			myList atPut(min, temp),
			"false"
		)
	)

	isFirst = true
	"Selection Sort: " print
	for(i, 0, (myList size) - 1, 
		if(isFirst,
			"" println
			isFirst = false,
			", " print
		)
		"#{myList at(i)}" interpolate print
	)
)

fib := method(n, 
	if(n == 1 or n == 2,
		return 1,
		return (fib(n-1) + fib(n-2))
	)
)

function5 := method(n, 
	for (i, 1, n-1,
		"Fib(#{i}) := #{fib(i)}" interpolate println
	)
)

function5a := method(a, b, n, i,
	if(a <= n,
		"Fib(#{i}) := #{a}" interpolate println
		return function5a(b, a+b, n, i + 1)
	)
)

if(isLaunchScript,
	"Assignment #4 - Io Day 1 - Sankar Roy" println
	"" println

	"Function1\n---------" println
	function1
	"" println
	
	"Function2\n---------" println
	function2("SearchMe", "Me")
	"" println
	function2("SearchMe", "They")
	"" println
	
	"Function3\n---------" println
	arrToUse := randArray(100)
	function3(arrToUse)
	"" println
	
	"Function4\n---------" println
	arrToUse2 := randArray(100)
	function4(arrToUse2)
	"\n" println
	
	"Function5\n---------\nTail Recursion" println
	function5(17)
	"\nAdding Up Recursion" println
	function5a(1, 1, 1000, 1)
	"" println
)
