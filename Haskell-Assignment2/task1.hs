--Task #1: (15 points) Write a Haskell function named mysort which can sort a list of items. You are not
--allowed to use the sort function available in any library. You can choose your favorite sort algorithm (e.g.
--quicksort) and implement it on your own. In addition to taking a list as input, mysort function should
--also take another comparison function that can be an anonymous function. Ensure that mysort works in
--general case, including the following examples.
--(a) mysort (<) [2,5,1]. The output should be [1,2,5]. (b) mysort (>) ["ab","cd","aa"]. The output should be
--["cd","ab","aa"] (c)mysort (\x y -> (size x < size y)) ["ab","c","baa"]. The output should be ["c","ab","baa"]

module Main where
  size x = length x

  --Do a quicksort, pass in f as the function for checking
  mysort :: (a -> a -> Bool) -> [a] -> [a]
  mysort f [] = []
  mysort f (h:t) = (mysort f (filter (not.f h) t) ) ++ [h] ++ (mysort f (filter (f h) t) )
