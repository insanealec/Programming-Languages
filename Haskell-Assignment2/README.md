Alexander Gordon

In order to run these you can load them in when the compiler is open.
What I usually do it just double-click on the compiler or open with the compiler and it loads everything in.

task1.hs:
  Sorts any list passed in, with a sort function, as so:
    *Main> mysort (<) [2,5,1]
    [1,2,5]
    *Main> mysort (>) ["ab", "cd", "aa"]
    ["cd","ab","aa"]
    *Main> mysort (\x y -> (size x < size y)) ["ab", "c", "baa"]
    ["c","ab","baa"]


task2.hs:
  Splits any numbers into a list of individual digits. If <= 0 return an empty list.
    *Main> toDigits 4323
    [4,3,2,3]
    *Main> toDigits 0
    []
    *Main> toDigits (-11)
    []
    *Main> toDigits 3979289183776022817
    [3,9,7,9,2,8,9,1,8,3,7,7,6,0,2,2,8,1,7]


task3.hs:
  part a:
   Sums the squares of all integers between 0 and 500
    input:
      sumSquares
    output:
      41941750
  

  part b:
   Put a string in as a parameter, get the same string, but all lowercase as a return.
    input:
      lowerCase "HELLO"
    output:
      "hello"

    input:
      lowerCase "hello"
    output:
      "hello"


  part c:
   Pass in a string, check if all values are lowercase.
    input:
      checkLower "HELLO"
    output:
      "All letters are lowercase."

    input:
      checkLower "hello"
    output:
      "Not all lowercase!"
