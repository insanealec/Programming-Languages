--Task #2: (15 points)Write a Haskell function that accepts an integer as the parameter, and if the integer is
--positive then it breaks it into its digits. The output is a list of digits. Use the following examples to test your
--function:
--(a) toDigits 4323 gives output [4,3,2,3] (b) toDigits 0 gives output [] (c) toDigits -11 gives output []

module Main where
  toDigits :: Int -> [Int]
  toDigits x = if (x <= 0) then [] else toDigits (x `div` 10) ++ [x `mod` 10]
