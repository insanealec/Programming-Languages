--Task #3: (20 points) Write Haskell code to do the following: (a) compute the sum of all odd (integers’)
--squares that are smaller than 500. (b) given a word, output the same word but with all letters mapped to
--uppercase, (c) given a word, check if all letters are in lowercase. Try to use higher-order functions, such as
--map, filter, etc. in your implementation.


module Main where
  --imported for toLower ability
  import Data.Char

  --(a) compute sum of all odd integers^2 that are less than 500 = 41941750
  sumSquares :: Integer
  sumSquares = foldl (+) 0 (map (^2) [1..499])

  --(b) input string, output all lowercase
  lowerCase :: [Char] -> [Char]
  lowerCase word = map (toLower) word

  --(c) get string, check if all are lowercase
  checkLower :: Monad m => [Char] -> m [Char]
  checkLower word = if (word == (filter (isLower) word)) then return "All letters are lowercase." else return "Not all lowercase!"