#matrixUtils.rb

require 'matrix'
require 'pp'

def randomMatrix(size)
	matrix = Matrix.build(size, size) {|row, col| rand(-100..100) }
	return matrix
end

def printMatrix(matrix)
	pp matrix
end