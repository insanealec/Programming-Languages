def function1
    # This function takes an array of numbers as the input and does the following:
    # (a) Uses 'each' method to print the elements, (b) Uses 'each_slice' method 
    # to print the elements while printing 5 elements at a time, (c) Uses 'select' method
    # to find the even integers, (d) Uses 'map' method to return the array of squares of the elements
    #, and (e) Uses 'inject' method to find the sum of all elements.
	puts "input numbers type x to exit"
	arr = []
	line = ""
	while line != "x"
		line = gets.chomp
		arr.push(line.to_i)
	end
	arr.pop
	
	puts "\neach \n"
	arr.each do |num|
		puts num
	end
	
	puts "\neach_slice by 5 \n"
	require 'enumerator'
	arr.each_slice(5) {|a| p a}
	
	puts "\nselect evens \n"
	arr.select do |num| 
		puts num if num % 2 == 0
	end
	
	puts "\nreturn array of sqares \n"
	a = []
	a = arr.map {|num| num ** 2}
	puts a.inspect
	
	puts "\ninject to find sum of all \n"
	a = arr.inject(0) {|sum, i| sum + i }
	puts a.inspect
end

def function2
    # Write a simple 'grep' function that will print the lines of a file having any
    # occurences of a phrase anywhere in that line. You need to do a simple regular expression 
    # match and read lines from a file. The output should also include the line numbers.
	puts "Enter a string to use as a regular expression."
	line1 = gets.chomp
	check = /#{line1}/
	i = 0
	aFile = File.open(File.dirname(__FILE__) + "/fun2.txt", "r") 
	aFile.each do |line|
		if check =~ line
			print "#{i} #{line}"
		end
		i = i+1
	end
	aFile.close
end

def function3a
    # Implement a function to calculate the Fibonacci Series iteratively. Calculate all Fibonacci numbers from 1 to 1000.
	max = 1000
	i1, i2 = 1, 1
	start = Time.now
	while i1 <= max
		puts i1
		i1, i2 = i2, i1 + i2
	end
	finish = Time.now
	puts "Time to loop = #{finish - start}"
end

def fibonacci( a, b, n )
    #n.times.each_with_object([0,1]) { |num, obj| obj << obj[-2] + obj[-1] }
	#n == 0? a : fibonacci(b, a+b, n - 1)
	if a <= n
		puts a
		fibonacci(b, a + b, n)
	else
		""
	end
end

def function3b
    # Implement a function to calculate the Fibonacci Series using recursion. Calculate all Fibonacci numbers from 1 to 1000.
    # Compare with function3a with respect to the computation time and lines of code.
	start = Time.now
	puts fibonacci( 1, 1, 1000 )
	finish = Time.now
	puts "Time to recursion = #{finish - start}"
	#from my tests this takes about the same amount of time, or at least unnoticeable
	#although I am able to do this in less lines of code
end
def function4
    # Implement a function to perform matrix multiplication. The function should take two parameters, Matrix P and Matrix Q
    # and return the resultant matrix, Matrix R.
	require_relative 'matrixUtils.rb'
	puts "Enter the size of the matrix: "
	size = gets.chomp.to_i
	matrixp = randomMatrix(size) #Matrix.build(10, 10) {|row, col| rand(-100..100) }
	matrixq = randomMatrix(size) #Matrix.build(10, 10) {|row, col| rand(-100..100) }
	puts "MatrixP:"
	printMatrix(matrixp)
	puts "\nMatrixQ:"
	printMatrix(matrixq)
	puts "\nMatrixR (the product of the matrices):"
	matrixr = matrixp * matrixq
	printMatrix(matrixr)
	#P = Matrix[ [25, 93], [-1, 66] ]
	#Q = Matrix[ [5, 3], [12, 10] ]
	#puts P.inspect
end
#have the tree class
class Tree
  attr_accessor :children, :node_name		
  def initialize(name, children=[])
    @children = children
    @node_name = name
  end
  def visit_all(&block)
    visit &block
    children.each {|c| c.visit_all &block}
  end
  def visit(&block)
    block.call self
  end
end
#now for hashes
class TreeHash
  attr_accessor :children, :node_name		
  def initialize(hash={})
    @children = []
	hash.each do |name, children|
		@node_name = name
		children.each do |cName, cChildren|
			@children.push(TreeHash.new({cName => cChildren}))
		end
	end
  end
  def visit_all(&block)
    visit &block
    @children.each {|c| c.visit_all &block}
  end
  def visit(&block)
    block.call self
  end
end

def function5
    # The Tree class presented in the textbook (Day 2) chapter is interesting, but it does not allow
    # you to specify a new tree with a clean user interface. Update the constructor and all other methods 
    # to accomodate the creation of a tree using a Hash. The initializer should accept a nested structure of Hashes. 
	hash_tree = TreeHash.new({'greatgrandpa' => {'grandpa1' =>{ 
			'parent1' => { 'child1' => { } , 'child2' => { } } ,
			'parent2' => { 'child3' => { } }
			} ,
		'grandpa2' =>
			{ 'parent3' => { 'child4' => { } } ,
			'parent4' => { 'child5' => { } , 'child6' => { } }
			}
		}
	})
	puts "Visiting a node."
	hash_tree.visit {|node| puts node.node_name}
	puts "\nVisiting entire tree."
	hash_tree.visit_all {|node| puts node.node_name}
end
if __FILE__ == $0
   
    function1
    puts ""
    function2
    puts ""
    function3a
    puts ""
    function3b
    puts ""
    function4
    puts ""
    function5
    puts ""

end


