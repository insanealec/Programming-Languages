/** Scala Assignment 1
  * Sam O'Neal
  * Alec Gordon
  */
  
object ScalaAssignment1
{
	def task1
	{
		println("The world is awesome.")
	}

	def task2(haystack: String, needle: String)
	{
		val index = haystack indexOf needle
		
		println(index)
	}
	
	def task3
	{
		// Random number generator object
		val r = scala.util.Random
		
		var original = new Array[Int](200)
		var randArray = original.map(x => r.nextInt(100))
		
		// Prints the array before the sort
		println("Before Sort:")
		randArray.foreach{arg =>
		print(s"$arg, ")
		}
		println()
		
		// Sorts the array by selection sort
		for(j <- 0 until (randArray.length - 1))
		{
			var min = j
			
			for(i <- (j + 1) until randArray.length)
			{
				if(randArray(i) < randArray(min))
				{
					min = i
				}
			}
			
			if(min != j)
			{
				val temp = randArray(min)
				randArray(min) = randArray(j)
				randArray(j) = temp
			}
		}
		
		// Prints the array after the sort
		println("After Sort:")
		randArray.foreach{arg =>
		print(s"$arg, ")
		}
		println()
	}
	
	def main(args: Array[String]): Unit =
	{
		// Task 1
		task1
		println()
		
		// Task 2
		task2("Find a string in this one", "ring")
		println()
		task2("Find a string in this one", "NoPe")
		println()
		
		// Task 3
		task3
		println()
	}
}