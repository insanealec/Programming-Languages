myMatrix := List clone;

myMatrix dim := method(x, y,
	self setSize(x); //rows
	for (i, 0, (x-1), 1,
		l := List clone;
		l setSize(y); //columns
		self atPut(i, l)
	)
)

myMatrix getElement := method(x, y,
	return self at(x) at(y)
)

myMatrix setElement := method(x, y, val,
	self at(x) atPut(y, val)
	return self
)

myMatrix randomMatrix := method(x, y,
	self dim(x, y)
	for(i, 0, x-1,
		for(j, 0, y-1,
			num := Random value(-100, 100) floor
			self setElement(i, j, num)
		)
	)
)


myMatrix printMatrix := method(matrix,
	for(i, 0, (matrix size)-1,
		for(j, 0, (matrix at(i) size)-1,
			//could use \t, but ends up being too big
			"    #{matrix getElement(i, j)}" interpolate print
		)
		"" println
	)
)

myMatrix add_matrix := method(matrix,
	p1 := self size
	q1 := self at(0) size //at 0 is okay, because all rows are same length
	p2 := matrix size
	q2 := matrix at(0) size
	if(p1 != p2 and q1 != q2,
		return "Matrix Orders Do Not Match! Addition Failed!",
		//create matrixC
		matC := myMatrix clone
		matC dim(p1, q1)
		for(i, 0, p1-1,
			for(j, 0, q1-1,
				res := (self getElement(i, j)) + (matrix getElement(i, j))
				matC setElement(i, j, res)
			)
		)
		return matC
	)
)

//test := myMatrix clone
//test randomMatrix(4, 5)
//test printMatrix(test)
//tester := myMatrix clone
//tester randomMatrix(4, 5)
//tester printMatrix(tester)
//newer := test add_matrix(tester)
//"" println
//newer printMatrix(newer)