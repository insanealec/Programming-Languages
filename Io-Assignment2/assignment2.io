doFile(System launchPath .."/matrixUtils.io")

//function to create a random list of "howMany" values
randArray := method(howMany,
	retArr := List clone
	for(i, 0, howMany, 
		num := Random value(300) floor;
		retArr append(num);
	)
	retArr
}

function1 := method(aList,
	//set the flag to know if the first loop or not, to display nicely
	isFirst := true
	
    //(a) Print the elements using a while loop
	"elements: " print
    i := 0
	while(i < (aList size) - 1, 
		if(isFirst,
			"" print
			isFirst = false,
			", " print
		)
		"#{aList at(i)}" interpolate print
		i = i + 1
	)
	"\n" println
	
	isFirst = true
	//(b) find the even integers (if any)
	"even integers present: " print
	for(i, 0, (aList size) - 1,
		if(aList at(i) isOdd,
			"do nothing",
			if(isFirst,
				"" print
				isFirst = false,
				", " print
			)
			"#{aList at(i)}" interpolate print
		)
	)
	"\n" println
	
	isFirst = true
	//(c) create a new list with squares of the elements of the given list
	"new list: " print
	sList := List clone
	sList setSize(aList size)
	for(i, 0, (aList size) - 1,
		sList atPut(i, (aList at(i) squared))
		if(isFirst,
			"" print
			isFirst = false,
			", " print
		)
		"#{sList at(i)}" interpolate print
	)
	"\n" println
	
	//(d) find the sum of all elements
	sum := 0
	for(i, 0, (aList size) - 1,
		sum = sum + (aList at(i))
	)
	"sum: #{sum}" interpolate println
)

//replacement of original add method
Number add_method := Number getSlot("+")
//the actual place '+' gets overridden
Number + := method(rightside,
	if(rightside type == "Sequence",
		sumOfChars := 0
		for(i, 0, (rightside size) - 1,
			sumOfChars = sumOfChars + (rightside at(i))
		)
		return self add_method(sumOfChars),
		return self add_method(rightside)
	)
)

function2 := method(
	//add two numbers
	(12 + 2.3) println
	
	//add a number and a string
	(12.1 + "abc") println

	"\n" println
)

function3 := method(matA, matB,

    //print the original matrices
    "Matrix A:" println
    matA printMatrix(matA)
    "" println
    "Matrix B:" println
    matB printMatrix(matB)

    //add the matrices
    matC := matA add_matrix(matB)

    //print the new matrix here if value returned
    "" println
    if(matC type == "Sequence",
    	matC println,
	    "Matrix C (matA+matB):" println
	    matC printMatrix(matC)
	)

    // return matC
    return matC
)


if(isLaunchScript,
    
    "Assignment #5 - Io Day 2 - S. Roy \n" println
	
	//generate the list of 100 random numbers
	f1List := randArray(100)
    "Function1" println
    "---------" println
	function1(f1List)
	"" println


    
    "Function2" println
    "---------" println
    function2
	"" println



    //create matrix A and matrix B
    matA := myMatrix clone
    matA randomMatrix(4,5)
    matB := myMatrix clone
    matB randomMatrix(4,5)
    "Function3" println
    "---------" println
    "4 by 5 addition" println
    function3(matA, matB)
	"" println

    //other runs of function 3
    matA randomMatrix(20,10)
    matB randomMatrix(20,10)
    "20 by 10 addition" println
    function3(matA, matB)
	"" println

	//this does work, it just too big for console window, uncomment if you'd like
   // matA randomMatrix(20,20)
   // matB randomMatrix(20,20)
   // "20 by 20 addition" println
   // function3(matA, matB)
   // "" println

    matA randomMatrix(10,20)
    matB randomMatrix(20,10)
    "10 by 20 not matching 20 by 10" println
    function3(matA, matB)
	"" println
     
)
